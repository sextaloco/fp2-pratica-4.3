/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author leandro
 */
public class Retangulo implements FiguraComLados {

    private double r;
    private double s;
    private double t;
    
    public Retangulo(double r, double s) {
        this.r = r;
        this.s = s;
    }
    
    public Retangulo(double r) {
        this.r = r;
        this.s = r;
        this.t = r;
    }
    
    @Override
    public double getLadoMenor() {
        return this.r < this.s ? this.r : this.s;
    }

    @Override
    public double getLadoMaior() {
        return this.r > this.s ? this.r : this.s;
    }

    @Override
    public String getNome() {
        return "Retângulo";
    }

    @Override
    public double getPerimetro() {
        return (this.r*2)+(this.s*2);
    }

    @Override
    public double getArea() {
        return this.r*this.s;
    }

    /**
     * @return the r
     */
    public double getR() {
        return r;
    }

    /**
     * @param r the r to set
     */
    public void setR(double r) {
        this.r = r;
    }

    /**
     * @return the s
     */
    public double getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(double s) {
        this.s = s;
    }

    /**
     * @return the t
     */
    public double getT() {
        return t;
    }

    /**
     * @param t the t to set
     */
    public void setT(double t) {
        this.t = t;
    }
    
}
