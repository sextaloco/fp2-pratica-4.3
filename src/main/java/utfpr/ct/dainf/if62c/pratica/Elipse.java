/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author leandro
 */
public class Elipse implements FiguraComEixos{

    private double r;
    private double s;

    /**
     * @return the r
     */
    public double getR() {
        return s;
    }

    /**
     * @param r the r to set
     */
    public void setR(double r) {
        this.r = r;
    }

    /**
     * @return the s
     */
    public double getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(double s) {
        this.s = s;
    }
    
    public Elipse(double semieixoR, double semieixoS) {
        this.r = semieixoR;
        this.s = semieixoS;
    }    
    
    public Elipse(double semieixoR) {
        this.r = semieixoR;       
    }
    
    @Override
    public double getEixoMenor() {
        return this.r < this.s?this.r:this.s;
    }

    @Override
    public double getEixoMaior() {
        return this.r > this.s?this.r:this.s;
    }

    @Override
    public String getNome() {
        return "Elipse";
    }

    @Override
    public double getPerimetro() {
        return Math.PI*((3*(this.r+this.s)-Math.sqrt((3*this.r+this.s)*(this.r+3*this.s))));        
    }

    @Override
    public double getArea() {
        return Math.PI*this.r*this.s;
    }
    
}
