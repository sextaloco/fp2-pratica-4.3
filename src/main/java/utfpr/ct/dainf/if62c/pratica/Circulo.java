/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author leandro
 */
public class Circulo extends Elipse{
     
    public Circulo(double semieixoR) {
        super(semieixoR, semieixoR);
    }
  
    @Override
    public String getNome() {
        return "Círculo";
    }
    
    @Override
    public double getPerimetro() {
        return Math.PI*getR()*2;        
    }
}

