
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leandro
 */
public class Pratica43 {
    public static void main(String[] args) {
        Quadrado quadrado = new Quadrado(5);
        Retangulo retangulo = new Retangulo(5,2);
        TrianguloEquilatero triangulo = new TrianguloEquilatero(5);
        System.out.println(quadrado.getArea());
        System.out.println(quadrado.getPerimetro());
        System.out.println(retangulo.getArea());
        System.out.println(retangulo.getPerimetro());
        System.out.println(triangulo.getArea());
        System.out.println(triangulo.getPerimetro());
        System.out.println(retangulo.getLadoMaior());
        System.out.println(retangulo.getLadoMenor());
        System.out.println(quadrado.getLadoMaior());
        System.out.println(quadrado.getLadoMenor());
        System.out.println(triangulo.getLadoMaior());
        System.out.println(triangulo.getLadoMenor());
    }
}
